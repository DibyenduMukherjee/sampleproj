Gitlabs needs to be integrated with modeler to provide the following functionalities. Bitbucket is currently supported by modeler and supports the below.

1. Format ssh url for Gitlabs.

2. Authentication using Basic and OAuth 2/3 legged.

    i. Basic or ApiKey based auth.

3. Search repositories by using file filter.

    i. List of repositories for current user and his teams.

4. Get branches on the server(remote), filter using exclusion list.

    i. List of branch names excluding given branches.

5. Create a remote branch on the server.

6. Gets a repository specific information.

7. List of current user repositories.

8. Gets the current user info.

9. Gets the last commitish on a branch.

    i. Gets the latest commit hash for a given file and branch.

10. Gets the file content of a file path for a specific branch.

    i. Gets the file content for a given file and branch.

11. Updates file content on a specific branch with the given data.

12. Assign branch level restrictions such as restrict merges, push, force delete and so on.

    i. Restrict merges on the branch. 

    ii. Restrict delete on the branch

    iii. Allow this api to be called only by repo admin.

13. Get branch restrictions for a given branch and kind. For e,g kind could be restrict_merges

14. Get the admin group and the members belonging to it.

15. Delete all branch restrictions associated with a branch.

16. Gets pull request for a given source branch and state. For e.g state could be OPEN.

17. Creates a pull request for a given src and target using the title and description given.

18. Perform pull request operation such as Merge, Approve, Decline. 

